package cellular;

import java.util.Random;
import java.util.function.Supplier;

import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain  extends GameOfLife {
   

    public BriansBrain(int rows, int columns) {
        super(rows,columns);
    }

     
    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = getCellState(row, col);
		

			if (state==CellState.ALIVE){return CellState.DYING;}
				
			if (state==CellState.DYING){return CellState.DEAD;}

			if (state==CellState.DEAD){
				if( countNeighbors(row, col,CellState.ALIVE)==2){return CellState.ALIVE;}
				else{return CellState.DEAD;}
					
				
		}

		return CellState.DEAD;		
	}
        

      

    }


