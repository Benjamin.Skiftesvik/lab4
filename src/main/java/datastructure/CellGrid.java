package datastructure;



import cellular.CellState;


public class CellGrid implements IGrid {

    int cols;
    int row;
    CellState[][] state;



    public CellGrid(int rows, int columns, CellState initialState) {
    
		this.row =rows;
        
        this.cols = columns;


        state = new CellState[row][cols];

        for(int i=0; i< rows ;i++){
            for (int j=0; j < columns;j++){
                state[i][j] = initialState;
                
               

            }
        }
        
        
        


	}

    @Override
    public int numRows() {
       
        return row;
       
    }

    @Override
    public int numColumns() {
        return cols;
        
    }

    @Override
    public void set(int row, int column, CellState element) {
        
       if (row < 0 || row >= numRows() || column < 0 || column >= numColumns()){ 

        throw new IndexOutOfBoundsException();
       }
       else{
           
           state[row][column] = element;
       }
        
    }

    @Override
    public CellState get(int row, int column) {

        if (row < 0 || row >= numRows() || column < 0 || column >= numColumns()){ 
            throw new IndexOutOfBoundsException();
       
       }
       else{
        return state[row][column];
           
       }
      
    }

    @Override
    public IGrid copy() {
        
        IGrid copy = new CellGrid(row, cols, CellState.ALIVE);

        for(int p = 0; p < row ;p++){
            for (int j= 0; j < cols ;j++){

            copy.set(p, j,state[p][j]); 
            
            
            }
        }

        return copy;

    }

}